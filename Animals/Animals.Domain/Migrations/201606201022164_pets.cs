using System.Data.Entity.Migrations;

namespace Animals.Domain.Migrations
{
    public partial class pets : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PetModels",
                c => new
                {
                    Id = c.Int(false, true),
                    Name = c.String(),
                    Age = c.Int(false),
                    Sex = c.String(),
                    PetType = c.String(false, 128)
                })
                .PrimaryKey(t => t.Id);
        }

        public override void Down()
        {
            DropTable("dbo.PetModels");
        }
    }
}