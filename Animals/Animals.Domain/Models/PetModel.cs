﻿namespace Animals.Domain.Models
{
    public abstract class PetModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Sex { get; set; }
    }
}