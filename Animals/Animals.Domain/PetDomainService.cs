﻿using System.Collections.Generic;
using System.Linq;
using Animals.Domain.Models;

namespace Animals.Domain
{
    public class PetDomainService
    {
        public void AddPetToDatabase(PetModel pet)
        {
            using (var db = new PetContext())
            {
                db.Pets.Add(pet);
                db.SaveChanges();
            }
        }

        public void DeletePetFromDatabase(int petId)
        {
            using (var db = new PetContext())
            {
                db.Pets.Remove(db.Pets.ToList()[petId]);
                db.SaveChanges();
            }
        }

        public List<PetModel> GetModelsFromDatabase()
        {
            var petModels = new List<PetModel>();
            using (var db = new PetContext())
            {
                petModels.AddRange(db.Pets);
            }
            return petModels;
        }

        public void EraseDatabase()
        {
            using (var db = new PetContext())
            {
                db.Database.ExecuteSqlCommand("TRUNCATE TABLE [animals].[dbo].[PetModels]");
                db.SaveChanges();
            }
        }
    }
}