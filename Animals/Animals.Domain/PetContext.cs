﻿using System.Data.Entity;
using Animals.Domain.Models;

namespace Animals.Domain
{
    public class PetContext : DbContext
    {
        public PetContext() : base("DbConnection")
        {
        }

        public DbSet<PetModel> Pets { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PetModel>()
                .Map<DogModel>(m => m.Requires("PetType").HasValue("D"))
                .Map<CatModel>(m => m.Requires("PetType").HasValue("C"))
                .Map<ElephantModel>(m => m.Requires("PetType").HasValue("E"));
        }
    }
}