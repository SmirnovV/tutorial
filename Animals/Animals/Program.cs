﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Animals.Classes;
using Animals.Classes.Pets;
using Animals.Classes.Reflection;
using Animals.Classes.Reflection.Attributes;
using Animals.Domain;
using Animals.Enums;
using static Animals.Enums.Sexes;

namespace Animals
{
    public class Program
    {
        private static readonly PetDomainService PetDomainService = new PetDomainService();
        private static readonly ModelMaker ModelMaker = new ModelMaker();

        private static List<Pet> _pets = new List<Pet>();

        public static void Main(string[] args)
        {
            var currentPetIndex = 0;

            _pets = ModelMaker.GetPetsListFromModels(PetDomainService.GetModelsFromDatabase());
            if (_pets.Count == 0)
            {
                _pets.Add(CreatePet());
                dynamic pet = _pets.Last();
                PetDomainService.AddPetToDatabase(ModelMaker.GetPetModel(pet));
            }
            else
            {
                ChangePet(_pets, out currentPetIndex);
            }
            Console.WriteLine("Now enter your command(? for help):");
            do
            {
                var command = Console.ReadLine();
                if (command == "0")
                {
                    return;
                }
                if (command == "m")
                {
                    ManagePets(ref _pets, ref currentPetIndex);
                }
                else
                {
                    ExecuteCommand(_pets[currentPetIndex], command);
                }
            } while (true);
        }

        public static Pet CreatePet()
        {
            var animals = Reflector.GetDescribedInheritors<Pet, DescriptionAttribute>().ToList();

            Console.WriteLine("Choose an animal:");

            for (var i = 0; i < animals.Count; i++)
            {
                var attribute = (DescriptionAttribute) animals[i].GetCustomAttribute(typeof (DescriptionAttribute));
                Console.WriteLine($"{i + 1} - {attribute.Description}");
            }

            int command;
            bool isCommandCorrect;

            do
            {
                isCommandCorrect = int.TryParse(Console.ReadLine(), out command);

                if (!isCommandCorrect || (command < 1) || (command > animals.Count))
                {
                    Console.WriteLine("Incorrect input. Try again:");
                    isCommandCorrect = false;
                }
            } while (!isCommandCorrect);

            var animalType = Type.GetType(animals[command - 1].ToString(), false, true);
            var pet = Reflector.InvokeConstructor(animalType);

            return pet as Pet;
        }

        private static void ExecuteCommand(Pet pet, string command)
        {
            var methodsDescriptions = Reflector.GetMethodsDescriptions(pet.GetType());

            if (command == "?")
            {
                ShowHelp(methodsDescriptions);
                return;
            }

            int methodNumber;

            if (int.TryParse(command, out methodNumber) && (methodNumber - 1 < methodsDescriptions.Count) &&
                (methodNumber - 1 >= 0))
            {
                Reflector.InvokeAttributedMethod(pet, methodsDescriptions, methodNumber);
            }
            else
            {
                Console.WriteLine("Incorrect input. Try again:");
            }
        }

        private static void ManagePets(ref List<Pet> pets, ref int currentPetIndex)
        {
            Console.WriteLine($"Current pet: {pets[currentPetIndex].Name}");
            Console.WriteLine("1. Change pet");
            Console.WriteLine("2. Give away this pet");
            Console.WriteLine("3. Create new pet");
            Console.WriteLine("4. Remove all pets");

            string command;

            do
            {
                command = Console.ReadLine();

                switch (command)
                {
                    case "1":
                        ChangePet(pets, out currentPetIndex);
                        break;
                    case "2":
                        RemovePet(pets, ref currentPetIndex);
                        break;
                    case "3":
                        pets.Add(CreatePet());
                        currentPetIndex = pets.Count - 1;
                        dynamic pet = _pets.Last();
                        PetDomainService.AddPetToDatabase(ModelMaker.GetPetModel(pet));
                        break;
                    case "4":
                        RemoveAllPets(pets);
                        currentPetIndex = 0;
                        break;
                }

                if (command != "1" && command != "2" && command != "3" && command != "4")
                {
                    Console.WriteLine("Incorrect input. Try again:");
                }
            } while (command != "1" && command != "2" && command != "3" && command != "3");
        }

        private static void ChangePet(List<Pet> pets, out int currentPetIndex)
        {
            Console.WriteLine("Available pets:");

            for (var i = 0; i < pets.Count; i++)
            {
                Console.WriteLine($"{i + 1}) {pets[i].Name} " +
                                  $"({((DescriptionAttribute) pets[i].GetType().GetCustomAttribute(typeof (DescriptionAttribute))).Description}" +
                                  $", {pets[i].Age} y. o., {pets[i].Sex})");
            }
            Console.WriteLine("Enter number to choose:");
            int command;
            do
            {
                if (!int.TryParse(Console.ReadLine(), out command) || command < 1 || command > pets.Count)
                {
                    Console.WriteLine("Incorrect input.Try again:");
                }
            } while (command < 1 || command > pets.Count);

            currentPetIndex = command - 1;

            Console.WriteLine($"{pets[currentPetIndex].Name} chosen.");
        }

        private static void RemovePet(List<Pet> pets, ref int currentPetIndex)
        {
            PetDomainService.DeletePetFromDatabase(currentPetIndex);
            pets.RemoveAt(currentPetIndex);
            if (pets.Count > 0)
            {
                ChangePet(pets, out currentPetIndex);
            }
            else
            {
                Console.WriteLine("Your pets list is empty!");
                pets.Add(CreatePet());
            }
        }

        private static void RemoveAllPets(List<Pet> pets)
        {
            pets.Clear();
            PetDomainService.EraseDatabase();
            Console.WriteLine("Your pets list is empty!");

            pets.Add(CreatePet());
        }

        public static void ShowHelp(List<string> methods)
        {
            Console.WriteLine("Available commands list:");
            Console.WriteLine("? - show help");

            for (var i = 0; i < methods.Count; i++)
            {
                Console.WriteLine($"{i + 1} - {methods[i]}");
            }
            Console.WriteLine("m - manage pets");
        }

        public static Sexes GetSex()
        {
            Console.WriteLine("Choose your pet sex:");
            Console.WriteLine("m - male");
            Console.WriteLine("f - female");

            Sexes? sex = null;

            do
            {
                switch (Console.ReadLine())
                {
                    case "m":
                        sex = Male;
                        break;
                    case "f":
                        sex = Female;
                        break;
                    default:
                        Console.WriteLine("Incorrect input. Try again:");
                        break;
                }
            } while ((sex != Male) && (sex != Female));

            return (Sexes) sex;
        }

        public static int GetAge(int maxLifespan)
        {
            int age;

            Console.WriteLine("Enter your pet age:");
            while (!int.TryParse(Console.ReadLine(), out age) || (age < 0) || (age > maxLifespan))
            {
                if (age < 0)
                {
                    Console.WriteLine("Incorrect value. Try again:");
                }
                else if (age > maxLifespan)
                {
                    Console.WriteLine("This animal can not live that long! Enter another value:");
                }
                else
                {
                    Console.WriteLine("Incorrect value. Try again:");
                }
            }

            return age;
        }

        public static string GetName()
        {
            Console.WriteLine("Enter your pet name:");

            string name;

            do
            {
                name = Console.ReadLine();
                if (name != null && name.Length < 2)
                {
                    Console.WriteLine("This word is too short for a real name. Try again:");
                }
            } while (name != null && name.Length < 2);

            return name;
        }
    }
}