﻿using System.Collections.Generic;
using System.Linq;
using Animals.Classes.Pets;
using Animals.Domain.Models;
using Animals.Enums;

namespace Animals.Classes
{
    public class ModelMaker
    {
        public PetModel GetPetModel(Dog pet)
        {
            return new DogModel {Age = pet.Age, Name = pet.Name, Sex = pet.Sex.ToString()};
        }

        public PetModel GetPetModel(Cat pet)
        {
            return new CatModel {Age = pet.Age, Name = pet.Name, Sex = pet.Sex.ToString()};
        }

        public PetModel GetPetModel(Elephant pet)
        {
            return new ElephantModel {Age = pet.Age, Name = pet.Name, Sex = pet.Sex.ToString()};
        }

        private Dog CreatePetFromModel(DogModel model)
        {
            return new Dog(model.Name, model.Age, model.Sex == "Male" ? Sexes.Male : Sexes.Female);
        }

        private Cat CreatePetFromModel(CatModel model)
        {
            return new Cat(model.Name, model.Age, model.Sex == "Male" ? Sexes.Male : Sexes.Female);
        }

        private Elephant CreatePetFromModel(ElephantModel model)
        {
            return new Elephant(model.Name, model.Age, model.Sex == "Male" ? Sexes.Male : Sexes.Female);
        }

        public List<Pet> GetPetsListFromModels(List<PetModel> models)
        {
            return models.Select(model => CreatePetFromModel((dynamic) model)).Cast<Pet>().ToList();
        }
    }
}