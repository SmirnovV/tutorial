﻿using System;
using System.Threading;
using Animals.Classes.Reflection.Attributes;
using Animals.Enums;

namespace Animals.Classes.Pets
{
    [Serializable]
    public abstract class Pet
    {
        protected Pet(string name, int age, Sexes sex, int maxLifespan,
            int pubertyAge, int oldAge, string genusName, string genusBabyName)
        {
            Name = name;
            Age = age;
            Sex = sex;

            if (Age < pubertyAge)
            {
                Console.WriteLine($"{genusBabyName}({sex}) created!");
            }
            else if ((Age <= oldAge) && (Age >= pubertyAge))
            {
                Console.WriteLine($"{genusName}({sex}) created!");
            }
            else if ((Age < maxLifespan) && (Age > oldAge))
            {
                Console.WriteLine($"Old {genusName}({sex}) created!");
            }
        }

        public string Name { get; set; }
        public int Age { get; set; }
        public Sexes Sex { get; set; }
        private bool IsAbleToPlay => Age <= 10;

        [Description("basic eating method")]
        protected void Feed(Nutritions nutrition, string food)
        {
            const int feedingDuration = 2000;
            var pronoun = Sex == Sexes.Male ? "his" : "her";
            var action = nutrition == Nutritions.Food ? "eats" : "drinks";

            Console.WriteLine($"{Name} {action} {pronoun} {food}...");
            Thread.Sleep(feedingDuration);
        }

        [Description("play")]
        public void Play()
        {
            const int playCounter = 5;
            const int playDelay = 1000;

            if (IsAbleToPlay)
            {
                int i;
                for (i = 0; i < playCounter; i++)
                {
                    Thread.Sleep(playDelay);
                    Console.WriteLine($"{Name} plays with you...");
                }
            }
            else
            {
                Console.WriteLine(
                    $"{GetType().Name} can play only if he is [0;10] years old. {Name}'s age - {Age} years");
            }
        }
    }
}