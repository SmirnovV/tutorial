﻿using System;
using System.Threading;
using Animals.Classes.Reflection.Attributes;
using Animals.Enums;

namespace Animals.Classes.Pets
{
    [Description("Dog")]
    [Serializable]
    public class Dog : Pet
    {
        public const int PubertyAge = 1;
        public const int OldAge = 10;
        public const int MaxLifespan = 20;


        public Dog(string name, int age, Sexes sex)
            : base(name, age, sex, MaxLifespan, PubertyAge, OldAge, "Dog", "Puppy")
        {
        }

        [Description("eat meat")]
        public void EatMeat()
        {
            var pronoun = Sex == Sexes.Male ? "his" : "her";
            Feed(Nutritions.Food, "dog food");
            Console.WriteLine($"{Name} licks {pronoun} mustache after delicious lunch.");
        }

        [Description("bark")]
        public void Bark()
        {
            const int maxWoofs = 11;
            const int minWoofDelay = 200;
            const int maxWoofDelay = 1000;
            var randomizer = new Random();
            var purrs = randomizer.Next(1, maxWoofs);
            int i;

            for (i = 0; i < purrs; i++)
            {
                Console.WriteLine("WOOF!");
                Thread.Sleep(randomizer.Next(minWoofDelay, maxWoofDelay));
            }
        }
    }
}