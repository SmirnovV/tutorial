﻿using System;
using System.Threading;
using Animals.Classes.Reflection.Attributes;
using Animals.Enums;

namespace Animals.Classes.Pets
{
    [Description("Elephant")]
    [Serializable]
    public class Elephant : Pet
    {
        public const int PubertyAge = 5;
        public const int OldAge = 60;
        public const int MaxLifespan = 80;

        public Elephant(string name, int age, Sexes sex)
            : base(name, age, sex, MaxLifespan, PubertyAge, OldAge, "Elephant", "Baby elephant")
        {
        }

        private bool IsAbleToRide => Age >= 5;

        [Description("eat fruits")]
        public void EatFruits()
        {
            var pronoun = Sex == Sexes.Male ? "his" : "her";
            Feed(Nutritions.Food, "fruits");
            Console.WriteLine($"{Name} finished {pronoun} lunch.");
        }

        [Description("ride")]
        public void Ride()
        {
            const int circuits = 5;
            const int rideDelay = 1000;
            if (IsAbleToRide)
            {
                Console.WriteLine($"You sat on {Name}'s back.");
                int i;
                for (i = 0; i < circuits; i++)
                {
                    Thread.Sleep(rideDelay);
                    Console.WriteLine($"You drove {i + 1} circuit!");
                }
            }
            else
            {
                Console.WriteLine($"You can ride on elephant's back only if he will older than 5 years.");
                Console.WriteLine($"{Name}'s age - {Age} years");
            }
        }
    }
}