﻿using System;
using System.Threading;
using Animals.Classes.Reflection.Attributes;
using Animals.Enums;

namespace Animals.Classes.Pets
{
    [Description("Cat")]
    [Serializable]
    public class Cat : Pet
    {
        public const int PubertyAge = 1;
        public const int OldAge = 10;
        public const int MaxLifespan = 30;

        public Cat(string name, int age, Sexes sex)
            : base(name, age, sex, MaxLifespan, PubertyAge, OldAge, "Cat", "Kitten")
        {
        }

        private bool IsAbleToEatFish => (Age <= 10) && (Age > 0);

        [Description("drink milk")]
        public void Eats()
        {
            var pronoun = Sex == Sexes.Male ? "his" : "her";
            Feed(Nutritions.Beverage, "milk");
            Console.WriteLine($"{Name} licks {pronoun} mustache after delicious lunch.");
        }

        [Description("pet him")]
        public void Purr()
        {
            const int maxPurrs = 11;
            const int purrDelay = 1000;
            var purrRandomizer = new Random();
            var purrs = purrRandomizer.Next(1, maxPurrs);
            int i;

            for (i = 0; i < purrs; i++)
            {
                Console.WriteLine($"{Name} purrs...");
                Thread.Sleep(purrDelay);
            }
        }

        [Description("eat fish")]
        public void EatFish()
        {
            var pronoun = Sex == Sexes.Male ? "his" : "her";

            if (IsAbleToEatFish)
            {
                Feed(Nutritions.Food, "fish");
                Console.WriteLine($"{Name} licks {pronoun} mustache after delicious lunch.");
            }
            else
            {
                Console.WriteLine($"Cat can eat fish only if he is [1;10] years old. {Name}'s age - {Age} years");
            }
        }
    }
}