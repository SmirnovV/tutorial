﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Animals.Classes.Pets;
using Animals.Classes.Reflection.Attributes;
using Animals.Enums;

namespace Animals.Classes.Reflection
{
    public class Reflector
    {
        public static List<string> GetMethodsDescriptions(Type obj)
        {
            var methods = obj.GetMethods();

            return methods.Select(
                   method =>
                          new
                          {
                              method,
                              attribute = (DescriptionAttribute) method.GetCustomAttribute(typeof (DescriptionAttribute))
                          })
                   .Where(methodWithAttribute => (methodWithAttribute.attribute != null) && methodWithAttribute.method.IsPublic)
                   .Select(methodWithAttribute => methodWithAttribute.attribute.Description).ToList();
        }

        public static IEnumerable<Type> GetDescribedInheritors<TBase, TAttribute>() where TAttribute : Attribute
        {
            var baseType = typeof (TBase);
            IEnumerable<Type> assemblyTypes = Assembly.GetAssembly(baseType).GetTypes();

            var inheritors =
                assemblyTypes.Where(inheritor => inheritor.IsSubclassOf(baseType) &&
                                                 inheritor.GetCustomAttribute(typeof (TAttribute)) != null);

            return inheritors;
        }

        public static void InvokeAttributedMethod(Pet pet, IReadOnlyList<string> methodsDescriptions, int methodNumber)
        {
            var methods = pet.GetType().GetMethods();

            foreach (var methodToInvoke in
                methods.Select(
                    method =>
                        new
                        {
                            method,
                            attribute = (DescriptionAttribute) method.GetCustomAttribute(typeof (DescriptionAttribute))
                        })
                    .Where(
                        methodWithAttribute =>
                            (methodWithAttribute.attribute != null) &&
                            (methodWithAttribute.attribute.Description == methodsDescriptions[methodNumber - 1]))
                    .Select(methodWithAttribute => pet.GetType().GetMethod(methodWithAttribute.method.Name)))
            {
                methodToInvoke.Invoke(pet, null);
                return;
            }
        }

        public static object InvokeConstructor(Type animalType)
        {
            var sex = Program.GetSex();
            var age = Program.GetAge((int) animalType.GetField("MaxLifespan").GetRawConstantValue());
            var name = Program.GetName();

            var parameterTypes = new Type[3];
            parameterTypes[0] = typeof (string);
            parameterTypes[1] = typeof (int);
            parameterTypes[2] = typeof (Sexes);

            var constructor = animalType.GetConstructor(parameterTypes);

            var parameters = new object[3];
            parameters[0] = name;
            parameters[1] = age;
            parameters[2] = sex;


            var pet = constructor?.Invoke(parameters);
            return pet;
        }
    }
}